Java 8 Stream allMatch, anyMatch and noneMatch methods are applied on stream object that
matches the given Predicate and then returns boolean value. allMatch() checks if calling
stream totally matches to given Predicate, if yes it returns true otherwise false. anyMatch()
checks if there is any element in the stream which matches the given Predicate. noneMatch() returns
true only when no element matches the given Predicate.
