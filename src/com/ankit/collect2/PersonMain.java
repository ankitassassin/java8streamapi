package com.ankit.collect2;

import java.util.*;
import java.util.stream.Collectors;

public class PersonMain
{
    public static void main(String[] args) {
        List<Person> persons =
                Arrays.asList(
                        new Person("Max", 18),
                        new Person("Pamela", 23),
                        new Person("David", 12));

       /* Collect is an extremely useful terminal operation to transform the elements of the stream into a
        different kind of result, e.g. a List, Set or Map. Collect accepts a Collector which consists of
        four different operations: a supplier, an accumulator, a combiner and a finisher.
        This sounds super complicated at first, but the good part is Java 8 supports various
        built-in collectors via the Collectors class. So for the most common operations
        you don't have to implement a collector yourself.*/

        List<Person> filtered = persons.stream().filter(e->e.name.startsWith("P")).collect(Collectors.toList());
        System.out.println(filtered);

        //for SET
        Set<Person> filteredSet = persons.stream().filter(e->e.name.startsWith("P")).collect(Collectors.toSet());
        System.out.println(filteredSet);

        //groupByAge
        Map<Integer,List<Person>> groupBy = persons.stream().filter(e->e.age>=18).collect(Collectors.groupingBy(e->e.age));
        System.out.println(groupBy);
        OptionalDouble avrg1 = persons.stream().map(e->e.age).mapToInt(e-> e).average();
        System.out.println("double" + avrg1.getAsDouble());
        //average age
        //another way of getting average
        Double averageAge = persons.stream().collect(Collectors.averagingInt(e->e.age));
        System.out.println(averageAge);

       //Transform stream into a map
        //In order to transform the stream elements into a map,
        // we have to specify how both the keys and the values should be mapped.

        Map<Integer,String> maps = persons.stream().collect(Collectors.toMap(e->e.age,e->e.name));
        System.out.println(maps);

        //if there are duplicate keys then do this:
        List<Person> persons1 =
                Arrays.asList(
                        new Person("Max", 18),
                        new Person("Peter", 23),
                        new Person("Pamela", 23),
                        new Person("David", 12));
        Map<Integer,String> maps1 = persons1.stream().collect(Collectors.toMap(e->e.age,e->e.name,(name1,name2)->name1+ ";" + name2));
        System.out.println(maps1);

        //how  to sort Person object
        //Using Comparator.comparingInt
        List<Person> sorted = persons1.stream().sorted(Comparator.comparingInt(Person::getAge).reversed()).collect(Collectors.toList());
        System.out.println("sorted persons are" + sorted);

        //another way using Comparator.comparing
        List<Person> sorted1 = persons1.stream().sorted(Comparator.comparing(Person::getAge).reversed()).collect(Collectors.toList());
        System.out.println("sorted1 persons are" + sorted1);


        //another way without using comparator
        List<Person> sorted2 = persons1.stream().sorted((e1,e2) -> e1.getAge() - e2.getAge()).collect(Collectors.toList());
        System.out.println("sorted2 persons are" + sorted2);


        //compare with more than one fields : age - descending order, name: ascending order
        List<Person> sorted3 = persons1.stream()
                .sorted(Comparator.comparing(Person::getAge).reversed()
                        .thenComparing(Person::getName))
                .collect(Collectors.toList());
        System.out.println("sorted3 persons are" + sorted3);

        //compare with more than one fields : both in descendng order
        //please notice, We are using Comparator for both age and name. While reversing list on multiple fields, we have to use comparator separately

        List<Person> sorted4 = persons1.stream()
                .sorted(Comparator.comparing(Person::getAge).reversed()
                        .thenComparing(Comparator.comparing(Person::getName).reversed()))
                .collect(Collectors.toList());
        System.out.println("sorted4 persons are" + sorted4);

        //increase age by 100 using constructor
        List<Person> increaseAgeBy100 = persons1.stream().map(e->new Person(e.getName(),e.getAge()+100)).collect(Collectors.toList());
        System.out.println(increaseAgeBy100);

        //increase age by 10 using setter
        System.out.println("using setter");
         persons1.forEach(e-> e.setAge(e.getAge()+1000));
        System.out.println(persons1);
        //usnig foreach
        persons1.forEach(System.out::println);





    }
}
