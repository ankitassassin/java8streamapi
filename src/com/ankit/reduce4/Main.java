package com.ankit.reduce4;

import com.ankit.collect2.Person;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Main
{
    public static void main(String[] args) {
        List<Person1> persons1 =
                Arrays.asList(
                        new Person1("Max", 18),
                        new Person1("Peter", 23),
                        new Person1("Pamela", 23),
                        new Person1("David", 12));

        //Why did I use Optinal. I was using just String, but error suggested to go with Optional
      Optional<Person1> name = persons1.stream().reduce((ele1, ele2)->ele1.age>ele2.age ?ele1:ele2);
        System.out.println(name.get());

        Optional<Person1> res = persons1.stream().reduce((ele1, ele2) -> ele1.age < ele2.age ? ele1:ele2);
        res.ifPresent(ele -> {
            System.out.println("=============");
            System.out.println(ele.name);
            System.out.println(ele.age);
            System.out.println("=============");
        });

        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
        Optional<Integer> a = numbers
                .stream()
                .reduce((subtotal, element) -> subtotal + element);
        System.out.println(a.get());

        //other way

        List<Integer> numbers1 = Arrays.asList(1, 2, 3, 4, 5, 6);
        int result = numbers1
                .stream()
                .reduce(0, (subtotal, element) -> subtotal + element);
        System.out.println(result);

        List<String> ls = Arrays.asList("ankit","kumar","singh");
        String p = ls.stream().reduce("",(ele1,ele2) -> ele1+ele2);
        System.out.println(p);

        String q = ls.stream().reduce("",String::concat);
        System.out.println(q);

        Optional<String> r = ls.stream().reduce(String::concat);
        System.out.println(r.get());
    }
}
