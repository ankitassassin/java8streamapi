package com.ankit.optional8;

import java.util.Optional;

/*Optional.isPresent() returns true if the given Optional object is non-empty. Otherwise it returns false.
Optional.ifPresent() performs given action in the predicate if the given Optional object is non-empty.
Otherwise it returns false.*/
public class OptionalIfPresentExample4
{
    public static void main(String[] args) {

        String a[] = new String[10];
        a[5] = "ankit";  // remove this one and run your program again
        Optional<String> checkNull = Optional.ofNullable(a[5]);
        if (checkNull.isPresent())
        {
            String word = checkNull.get().toUpperCase();
            System.out.println(word);
        }
        else {
            System.out.println("word is null");
        }

        checkNull.ifPresent(g-> System.out.println(checkNull.get().toUpperCase()));
    }
}
