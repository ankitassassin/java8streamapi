package com.ankit.optional8;

import java.util.Optional;

/*Optional.ofNullable() method returns a Non-empty Optional if a value present in the given object.
Otherwise returns empty Optional.
Optionaal.empty() method is useful to create an empty Optional object.*/
public class OptionalBasicExample1 {

    public static void main(String[] args) {

        Optional<String> gender = Optional.of("MALE");
        String answer1 = "Yes";
        String answer2 = null;

        System.out.println("Non-Empty Optional:" + gender);  //Non-Empty Optional:Optional[MALE]
        System.out.println("Non-Empty Optional: Gender value : " + gender.get()); //Non-Empty Optional: Gender value : MALE
        System.out.println("Empty Optional: " + Optional.empty()); //Empty Optional: Optional.empty

        System.out.println("ofNullable on Non-Empty Optional: " + Optional.ofNullable(answer1)); // ofNullable on Non-Empty Optional: Optional[Yes]
        System.out.println("ofNullable on Empty Optional: " + Optional.ofNullable(answer2)); //ofNullable on Empty Optional: Optional.empty

        // java.lang.NullPointerException
        System.out.println("ofNullable on Non-Empty Optional: " + Optional.of(answer2));

    }

}
