package com.ankit.optional8;

import java.util.Optional;

public class OptionalMapFlapMapExample2 {

    public static void main(String[] args) {

        Optional<String> nonEmptyGender = Optional.of("male");
        Optional<String> emptyGender = Optional.empty();

        System.out.println("Non-Empty Optional:: " + nonEmptyGender.map(String::toUpperCase)); //Non-Empty Optional:: Optional[MALE]
        System.out.println("Empty Optional    :: " + emptyGender.map(String::toUpperCase)); //Empty Optional : Optional.empty

        Optional<Optional<String>> nonEmptyOtionalGender = Optional.of(Optional.of("male"));
        System.out.println("Optional value   :: " + nonEmptyOtionalGender); //Optional value   :: Optional[Optional[male]]
        System.out.println("Optional.map     :: " + nonEmptyOtionalGender.map(gender -> gender.map(String::toUpperCase))); //Optional.map :: Optional[Optional[MALE]]
        System.out.println("Optional.flatMap :: " + nonEmptyOtionalGender.flatMap(gender -> gender.map(String::toUpperCase))); //Optional.flatMap :: Optional[MALE]

    }

}
