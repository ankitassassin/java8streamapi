package com.ankit.optional8;

public class OldSwitchClass0
{
    public static void main(String[] args) {
        String a = "M";
        switch (a) {
            case "M":
                System.out.println("It's monday");
                break;

            case "S":
                System.out.println("It's sunday");
                break;

            default:
                System.out.println("default");
                break;
        }

      /*  Only supported in java 14
        String newVar = "S";
        String res = switch (newVar){
            case "M" -> "asd";
            case "N" -> "asd";
        }*/

        /*
        scala> number match {
               case 1 => println("It's 1")
               case 2 => println("It's 2")
               case _ => println("It's neither 1 nor 2")
	 }
         */
    }
}
