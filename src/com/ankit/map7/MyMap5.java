package com.ankit.map7;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MyMap5
{
    public static void main(String[] args) {

        Map<String, String> books = new HashMap<>();
        books.put(
                "978-0201633610", "Design patterns : elements of reusable object-oriented software");
        books.put(
                "978-1617291999", "Java 8 in Action: Lambdas, Streams, and functional-style programming");
        books.put("978-0134685991", "Effective Java");
        books.put("978-0321356680", "Effective Java: Second Edition");

       List<String> res =  books.entrySet().stream().map(e->{
            if (e.getKey().equalsIgnoreCase("978-1617291999")){
                return e.getKey();
            }

            else {
                return e.getValue();
            }
        }).collect(Collectors.toList());

        System.out.println(res);
    }
}
