package com.ankit.map7;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyMap1
{
    public static void main(String[] args) {
        Map<String, Integer> someMap = new HashMap<>();
        someMap.put("ankit",31);
        someMap.put("mayank",31);
        someMap.put("shashank",29);
        someMap.put("ashu",20);

        //traditional way to print
        for(Map.Entry<String,Integer> hm : someMap.entrySet()){
            System.out.println(hm.getKey() + ":::" + hm.getValue());
        }
        //We can obtain a set of key-value pairs:
        Set<Map.Entry<String, Integer>> entries = someMap.entrySet();
        System.out.println(entries);
        //We can also get the key set associated with the Map:
        Set<String> keySet = someMap.keySet();
        System.out.println(keySet);
        //to get values
        Collection<Integer> values = someMap.values();

        //These each give us an entry point to process those collections by obtaining streams from them:
        Stream<Map.Entry<String, Integer>> entriesStream = entries.stream();
        Stream<Integer> valuesStream = values.stream();
        Stream<String> keysStream = keySet.stream();

        System.out.println("print");
        someMap.entrySet().stream().forEach(e->{
            System.out.println(e.getKey());
            System.out.println(e.getValue());
        });

        /*how to sort it based on value - way 1
          Please note that we are using LinkedHashMap::new while sorting, so that once we sort it and then convert it into map, it
          maintains the sorting order.
         */

        Map<String,Integer> map = someMap.entrySet().stream()
                .sorted((Map.Entry<String,Integer> o1,Map.Entry<String,Integer> o2) -> o1.getValue().compareTo(o2.getValue()))
                .collect(Collectors.toMap(e1->e1.getKey(), e2->e2.getValue(),(e3, e4)->e3, LinkedHashMap::new));
        System.out.println(map);

        //how to sort it based on value - way 2 (Map.Entry::getKey or Map.Entry::getKey)
        Map<String,Integer> map1 = someMap.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .collect(Collectors.toMap(e1->e1.getKey(), e2->e2.getValue(),(e3, e4)->e3, LinkedHashMap::new));
        System.out.println(map1);

        /*Please note that in map Comparator.comparing(Map.Entry::getKey).reversed() doesn't work, it will throw error.
          You will have to do as below:
          Map.Entry.<String,Integer>comparingByKey().reversed() or
          Collections.reverseOrder(Comparator.comparing(Map.Entry::getKey))

         */

        Map<String,Integer> map2 = someMap.entrySet().stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(Map.Entry::getKey)))
                .collect(Collectors.toMap(e1->e1.getKey(), e2->e2.getValue(),(e3, e4)->e3, LinkedHashMap::new));
        System.out.println(map2);


        Map<String,String> test = new HashMap<>();
        test.put("abc","234");
        test.put("cde","123");
        test.put("fgh","NA");
        List<String> output = test.entrySet().stream().filter(e->!"NA".equalsIgnoreCase(e.getValue()))
                .map(Map.Entry::getKey).collect(Collectors.toList());
        System.out.println(output);

    }
}
