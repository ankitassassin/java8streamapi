package com.ankit.map7;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MyMap6
{
    public static void main(String[] args) {

        //printing map elements as string
        Map<String,String> webReqParam = new HashMap<>();
        webReqParam.put("ankit","sngh");
        String value = webReqParam.entrySet().stream().
                map(key -> key.getKey() + "=" + key.getValue()).collect(Collectors.joining(",", "{","}"));
        System.out.println(value);

    }
}
