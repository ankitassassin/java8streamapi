package com.ankit.map7;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyMap4
{

    public static void main(String[] args) {
        //sorting map based on values
        Map<String,Integer> map1 = new HashMap<>();
        map1.put("A",9);
        map1.put("B",2);
        map1.put("C",4);
        map1.put("D",10);
        System.out.println(map1);

        /*in the below code (e3,e4)->e3 is a merge function
        merge function(consider the first key if there is a duplicate key)*/
        Map res1 = map1.entrySet().stream().sorted((Map.Entry<String,Integer> o1,Map.Entry< String,Integer> o2)->o2.getValue().compareTo(o1.getValue())).
                collect(Collectors.toMap(e1->e1.getKey(),e2->e2.getValue(),(e3,e4)->e3,LinkedHashMap::new));
        //In the same way we can also sort map byb using key
        System.out.println(res1);

        //By using lamda expresion
        Map res =  map1.entrySet().stream().sorted(Map.Entry.<String,Integer>comparingByKey().reversed()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        System.out.println(res);








    }

}
