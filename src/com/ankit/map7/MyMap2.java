package com.ankit.map7;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class MyMap2
{
    public static void main(String[] args) {
        Map<String, String> books = new HashMap<>();
        books.put(
                "978-0201633610", "Design patterns : elements of reusable object-oriented software");
        books.put(
                "978-1617291999", "Java 8 in Action: Lambdas, Streams, and functional-style programming");
        books.put("978-0134685991", "Effective Java");
        books.put("978-0321356680", "Effective Java: Second Edition");

        //We are interested in finding the ISBN for the book with the title “Effective Java”.
        Optional<String> optionalIsbn = books.entrySet().stream()
                .filter(e -> "Effective Java".equals(e.getValue()))
                .map(e->e.getKey())
                .findFirst();
        System.out.println(optionalIsbn.get());

        List<String> multipleBooks = books.entrySet().stream()
                .filter(e -> e.getValue().startsWith("Effective Java"))
                .map(Map.Entry::getKey)  //or .map(e -> e.getKey())
                .collect(Collectors.toList());
        System.out.println(multipleBooks);

        //Getting a Map‘s Values Using Streams
        List<String> titles = books.entrySet().stream()
                .filter(e -> e.getKey().startsWith("978-0"))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
        System.out.println(titles);

    }
}
