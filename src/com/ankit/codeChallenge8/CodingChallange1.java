package com.ankit.codeChallenge8;

import java.util.*;
import java.util.stream.Collectors;


public class CodingChallange1
{
    /*
    -- Write a program to find the duplicate characters in list of string . Only java stream will be accepted ?
   */
    public static void findDuplicateChar(String s){
        Map<String,Long> map = Arrays.stream(s.split(""))
                .collect(Collectors.groupingBy(e->e,Collectors.counting()))
                .entrySet().stream().filter(e->e.getValue() > 1).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        System.out.println(map);
       //we can also write collect(Collectors.toMap(e->e.getKey(),e->e.getValue()))

    }

    //find Nth salary in a map
    public static void findNthSalary(Map<String,Integer> student,int nth)
    {
       //I have written this just to show, how the output will look like
        System.out.println(student.entrySet().stream()
                .collect(Collectors.groupingBy(e->e.getValue(),Collectors.mapping(e->e.getKey(),Collectors.toList()))));
        //{400=[Rinku], 100=[vivek], 500=[shankar, shyam], 600=[vishal], 300=[tinku]}

        System.out.println(student.entrySet().stream()
                        .collect(Collectors.groupingBy(e->e.getValue(),Collectors.mapping(e->e.getKey(),Collectors.toList())))
                        .entrySet().stream().sorted(Collections.reverseOrder(Comparator.comparing(Map.Entry::getKey))).collect(Collectors.toList()));

        System.out.println(student.entrySet().stream()
                .collect(Collectors.groupingBy(e->e.getValue(),Collectors.mapping(e->e.getKey(),Collectors.toList())))
                .entrySet().stream().sorted(Collections.reverseOrder(Comparator.comparing(Map.Entry::getKey))).collect(Collectors.toList()).get(nth-1));

    }

    //findFirstNonRepeatnigEleFromArray
    public static void findFirstNonRepeatingEle(String s){
        System.out.println(
                Arrays.stream(s.split(""))
                .collect(Collectors.groupingBy(e->e,LinkedHashMap::new,Collectors.counting()))
                .entrySet().stream().filter(e->e.getValue()==1).map(e->e.getKey()).collect(Collectors.toList()).get(0)
        );
    }

    //find second highestEle from Array
    public static void findSecondHighestEle(int a[]){

        /*
        int first_highest = a[0];
        int secondHighest = a[0];
        for(int i = 1;i<a.length;i++){
            if(a[i] > first_highest){
                secondHighest = first_highest;
                first_highest = a[i];
            } else if (a[i] > secondHighest) {
                secondHighest = a[i];
            }
        }
        System.out.println("first highest: " + first_highest + ", second highest: " + secondHighest);
        */

        /*
        When we use Arrays.stream(a on an array of primitives, it returns a primimtive
        stream(like IntStream,DoubleStream etc.).
        In this case, if you want to compare or perform operations that require objects(like Integer, Double),
        you will need to use boxed to convert the primitive stream to a stream of wrapper objects.
         */

        System.out.println(Arrays.stream(a).boxed().distinct().sorted(Comparator.reverseOrder()).skip(1).findFirst().get());

    }

    //find longestString from a given array of String
    public static void findLongestString(String a[]){
        System.out.println(Arrays.stream(a).reduce((word1,word2) -> word1.length() > word2.length() ?word1:word2).get());
    }

    //Write a Java program to find all elements from array which starts from 2 using stream API.
    public static void findDigitsStartwith2(int a[]){
        System.out.println(Arrays.stream(a).mapToObj(e->String.valueOf(e)).filter(e->e.startsWith("2")).collect(Collectors.toList()));;
    }



    public static void main(String[] args) {
        findDuplicateChar("ankita");

        Map<String, Integer> map = new HashMap<>();
        map.put("vivek", 100);
        map.put("Rinku", 400);
        map.put("vishal", 600);
        map.put("shankar", 500);
        map.put("shyam", 500);
        map.put("tinku", 300);

        findNthSalary(map,2);

        findFirstNonRepeatingEle("ankitank");
        findSecondHighestEle(new int[]{1,3,2,4,5,6,6,9,9,10,11});
        findLongestString(new String[]{"vivek"  , "kadiyan" , "ram" , "chaudhary vivek kadiyan"});
        findDigitsStartwith2(new int[]{1,25,34,45,26,26,37});
    }

}
