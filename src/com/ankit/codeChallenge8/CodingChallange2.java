package com.ankit.codeChallenge8;

import java.util.ArrayDeque;
import java.util.Deque;


/*
Stack offers LIFO type operation, and queue offers FIFO type operation. But on the other hand,
deque gives user the flexibility to add/remove elements from both it's end — front and back.
Thereby deque can mimic the operations of both stack and queue if required.

Stack class is synchronized . On the other hand, the Deque interface is not thread-safe.
So, if thread-safety is not a requirement, a Deque can bring us better performance than a Stack.

Queue can be implemented with LinkedList or PriorityQueue
Deque can be implemented with LinkedList or ArrayDeque
 */
public class CodingChallange2
{
    public static boolean areParanthesisBalanced(String expr){
        Deque<Character> stack = new ArrayDeque<>();  // you can also use Stack<Integer> stack = new Stack<>();

        for(int i = 0;i<expr.length();i++){
            char x = expr.charAt(i);

            //if current character is an opening bracket, push it to stack
            if ( x == '(' || x == '{' || x == '[')
            {
                stack.push(x);
                continue;
            }
            //if current character is not opening bracket, stack can't be empty
            if(stack.isEmpty()){
                return false;
            }


            switch (x){
                case ')' :
                    if(stack.pop() != '('){
                        return false;
                    }
                    break;
                case '}' :
                    if(stack.pop() != '{'){
                        return false;
                    }
                    break;
                case ']' :
                    if(stack.pop() != '['){
                        return false;
                    }
                    break;
            }

        }
        return stack.isEmpty();
    }
    public static void main(String[] args) {
        String inputString = "[()]{}{[()()]()}";  //should be true as all the brackets are well-formed
        System.out.println(areParanthesisBalanced(inputString));

        String inputString1 = "[(])";  //should be false. 1 and 4 brackets are not balanced because there is a closing ‘]’ before the closing ‘(‘
        System.out.println(areParanthesisBalanced(inputString1));
    }
}
