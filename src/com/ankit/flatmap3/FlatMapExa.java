package com.ankit.flatmap3;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlatMapExa
{
    public static void main(String[] args) {
        /*flatMap() is the combination of a map and a flat operation i.e,
         it first applies map function and than flattens the result*/
        List<String> list = Arrays.asList("5.6", "7.4", "4",
                "1", "2.3");
        list.stream().flatMap(num -> Stream.of(num)).
                forEach(System.out::println);

        List<String> list1 = Arrays.asList("Geeks", "GFG",
                "GeeksforGeeks", "gfg");

        list1.stream().flatMap(str ->
                Stream.of(str.charAt(2))).
                forEach(System.out::println);

        list1.stream().map(str ->
                str.charAt(2)).
                forEach(System.out::println);

        //Convert list of lists to single list
        List<Integer> list4 = Arrays.asList(1,2,3);
        List<Integer> list2 = Arrays.asList(4,5,6);
        List<Integer> list3 = Arrays.asList(7,8,9);

        List<List<Integer>> listOfLists = Arrays.asList(list3, list2, list4);

        List<Integer> listOfAllIntegers = listOfLists.stream()
                .flatMap(x -> x.stream())
                .collect(Collectors.toList());

        System.out.println(listOfAllIntegers);

        //Convert array of arrays to single list
        String[][] dataArray = new String[][]{{"a", "b"}, {"c", "d"}, {"e", "f"}, {"g", "h"}};

        List<String> listOfAllChars = Arrays.stream(dataArray)
                .flatMap(x -> Arrays.stream(x))
                .collect(Collectors.toList());

        System.out.println(listOfAllChars);

    }
}
