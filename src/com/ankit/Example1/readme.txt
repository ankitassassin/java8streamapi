Q1. What New Features Were Added in Java 8?
Java 8 ships with several new features but the most significant are the following:

--> Lambda Expressions − a new language feature allowing treating actions as objects
--> Method References − enable defining Lambda Expressions by referring to methods directly using their names
--> Optional − special wrapper class used for expressing optionality
--> Functional Interface –
    A functional interface is an interface that contains only one abstract method can have any number of default and static methods.
    You can use @FunctionalInterface to annotate a functional interface. In that case, compiler will verify if the interface actually contains just one abstract method or not.
    From Java 8 onwards, lambda expressions can be used to represent the instance of a functional interface.
	Functional interfaces are included in Java SE 8 with Lambda expressions and Method references in order to make code more readable, clean, and straightforward.

  -> befor java 8:
  // create anonymous inner class object
        new Thread(new Runnable() {
            @Override public void run()
            {
                System.out.println("New thread created");
            }
        }).start();

  -> after java 8:
     // lambda expression to create the object
        new Thread(() -> {
            System.out.println("New thread created");
        }).start();


	==> @FunctionalInterface annotation is used to ensure that the functional interface can’t have more than one abstract method. In case more than one abstract methods are present, the compiler flags an ‘Unexpected @FunctionalInterface annotation’ message. However, it is not mandatory to use this annotation.

	Since Java SE 1.8 onwards, there are many interfaces that are converted into functional interfaces. All these interfaces are annotated with @FunctionalInterface. These interfaces are as follows –

		Runnable –> This interface only contains the run() method.
		Comparable –> This interface only contains the compareTo() method.
		ActionListener –> This interface only contains the actionPerformed() method.
		Callable –> This interface only contains the call() method.

    @FunctionalInterface
    interface Square
    {
        int calculate(int x);   //in interface, the abstract method don't need to be explicitely declared as abstarct.
		                        //Any method in interface is by default abstarct unless it's a default method or static method
    }

    class Test
    {
        public static void main(String args[])
        {
            int a = 5;

            // lambda expression to implement abstarct method.
            Square s = (int x)->x*x;

            // parameter passed and return type must be
            // same as defined in the prototype
            int ans = s.calculate(a);
            System.out.println(ans);
        }
    }


--> Default and static methods − give us the ability to add full implementations in interfaces besides abstract methods
--> Stream API − a special iterator class that allows processing collections of objects in a functional manner
--> Date API − an improved, immutable JodaTime-inspired Date API


Q1. What Is a Method Reference?
A method reference is a Java 8 construct that can be used for referencing a method without invoking it. It is used for treating methods as Lambda Expressions. They only work as syntactic sugar to reduce the verbosity of some lambdas. This way, the following code:

There are four variants of method references.

1. Reference to a Static Method
   The reference to a static method holds the following syntax: ContainingClass::methodName.
   boolean isReal = list.stream().anyMatch(User::isRealUser);

2. Reference to an Instance Method
   The reference to an instance method holds the following syntax: containingInstance::methodName
   User user = new User();
   boolean isLegalName = list.stream().anyMatch(user::isLegalName);

3. Reference to an Instance Method of an Object of a Particular Type
   long count = list.stream().filter(String::isEmpty).count();

4. Reference to a Constructor
   A reference to a constructor takes the following syntax: ClassName::new
   Stream<User> stream = list.stream().map(User::new);


Q2. What Is the Meaning of String::Valueof Expression?
It is a static method reference to the valueOf method of the String class.

Q3: Difference between Collction and Stream ?
Ans: Collection is used to store the element but stream is not. It is used to perform opeartin stored in collection.

Q4: What is the use of map ?
Ans: map transform one type of object into another by applying function.
     Exa: List of strinng can be converted to List of integer using map by using parseInt function.

Q5: Use of flatMap ?
Ans: It is an extension of map. Apart from transforming one object into another, it can also flatten it.
     exa: If you have list of the list but you want to combine all elements into one list, you can use flatMap here.

Q6: What does filter do ?
Ans: filter elemnets from collection that satisfy a certain condition that is specified using Predicate condition.

Q7: Difference between intermediate and terminal operation ?
Ans: Intermediate opeartion returns another stream, which means you can further call other methods of Stream
     class to compose a pipeline. exa: map, filter, flatmap etc.

      While terminal opeartion produces result like a value or a Collection.
      exa: collect, forEach

Q8: What do you mean by "Stream is lazy" ?
Ans: When we say that Stream is lazy we mean that most of the methods defined byStream class is lazy.
     They will only work when you call a terminal method on Stream pipeline.

Q9: what is paralllel stream ?
Ans: Parallel stream uses multiple thread for operation.