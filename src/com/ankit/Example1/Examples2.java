package com.ankit.Example1;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Examples2
{
    public void meth()
    {
        /*Besides regular object streams Java 8 ships with special kinds of streams for working
        with the primitive data types int, long and double. As you might have guessed
        it's IntStream, LongStream and DoubleStream.*/

        //IntStreams can replace the regular for-loop utilizing IntStream.range():
        IntStream.range(1, 4)
                .forEach(System.out::println);

        Arrays.stream(new int[] {1, 2, 3})
                .map(n -> 2 * n + 1)
                .average()
                .ifPresent(System.out::println);

        List<Integer> da = Arrays.asList(1,2,3,4);
        Stream<Integer> str = da.stream().map(e->e*2);
        List<Integer> l1 = str.collect(Collectors.toList());

        /*Note: sum(), average() method only supports primitive type streams.
              If we want to use a stream on a boxed Integer value, we must first convert
              the stream into IntStream using the mapToInt method.*/
        List<Integer> data = Arrays.asList(1,2,3,4);
        OptionalDouble a = data.stream().map(e->e*2).mapToInt(e-> e).average();
        System.out.println(a.getAsDouble());


        Stream.of("a1", "a2", "a3")
                .map(s -> s.substring(1))
                .mapToInt(Integer::parseInt )
                .max()
                .ifPresent(System.out::println);

        IntStream.range(1, 4)
                .mapToObj(i -> "a" + i)
                .forEach(System.out::println);

        Stream.of(1.0, 2.0, 3.0)
                .mapToInt(Double::intValue)
                .mapToObj(i -> "a" + i)
                .forEach(System.out::println);

        Stream.of("d2", "a2", "b1", "b3", "c")
                .map(e -> {
                    System.out.println("map is:" + e);
                    return e;
                }).filter(s -> {
                    System.out.println("filter: " + s);
                    return true;
                })
                .forEach(s -> System.out.println("forEach: " + s));

                      /*   output:
                            map is:d2
                            filter: d2
                            forEach: d2
                            map is:a2
                            filter: a2
                            forEach: a2
                            map is:b1
                            filter: b1
                            forEach: b1
                            map is:b3
                            filter: b3
                            forEach: b3
                            map is:c
                            filter: c
                            forEach: c*/
        /*
          By looking at above output we can say that in Java8 Streams, operations like map,filter,foreach are typically
          applied lazily and in a pipeline fashion. This means that each element of the stream passes through the
          pipeline of operations  one by one.
          Each element of the stream is processed through entire pipeline of intermediate operations(map, filter etc)
          before moving to the next element. This type of processins is called vertical processing.
          Horizontal processing something where all elements will processed through a opearation before moving to
          next opeartion. exa: sorted. You can find this n example5.
         */



    }
}
