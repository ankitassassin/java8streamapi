package com.ankit.Example1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Difference between map and foreach:


## .map
==> The .map method is used to transform elements in a stream. It applies function to each element and returns a new stream with transformed elements.
==> It always return new stream consisting of the transformed elements.
==> It's used when you need to apply some trandformation to eacch element of the stream.

## .forEach
==> the .forEach method is used to perform an "action" for each element of the stream. exa: printing of elements.
==> It always return void means, it doesn't return a new stream or any other value.
==> It's used when you want to perform an action on each element of the stream and don't need to collecct the results.
*/
public class Examples1
{
    public void meth()
    {
        List<String> myList =
                Arrays.asList("a1", "a2", "b1", "c2", "c1");

        //first way

        /* sorted method will sort the data in increasing order. We know that Collection.sort() method sort the data in increasng order by default.
        You can also provide sorted(String::compareTo) and will sort in increasing order. But we know that to sort the data in decreasing order we need to
        implement comparator interface. So here also we will do it in the same way    */

        myList.stream().filter(ele -> ele.startsWith("c")).
                map(ele->ele.toUpperCase()).sorted().forEach(ele-> System.out.println(ele));

        myList.stream().filter(ele -> ele.startsWith("c")).
                map(String::toUpperCase).sorted(String::compareTo).forEach(ele-> System.out.println(ele));

        myList.stream().filter(ele -> ele.startsWith("c")).
                map(ele->ele.toUpperCase()).sorted(Comparator.reverseOrder()).forEach(ele-> System.out.println(ele));

        //second way
        myList.stream().filter(ele -> ele.startsWith("c")).
                map(String::toUpperCase).sorted().forEach(System.out::println);

        //List output
        List<String> res = myList.stream().filter(ele->ele.startsWith("a")).map(String::toUpperCase).collect(Collectors.toList());
        System.out.println(res);

        /*Streams can be created from various data sources, especially collections.
        Lists and Sets support new methods stream() and parallelStream() to either create a sequential
        or a parallel stream. Parallel streams are capable of operating on multiple threads and will be
        covered in a later section of this tutorial. We focus on sequential streams for now:*/

        Arrays.asList("a1", "a2", "a3")
                .stream()
                .findFirst()
                .ifPresent(System.out::println);

        // we don't have to create collections always in order to work with streams

        Stream.of("a1", "a2", "a3")
                .findFirst()
                .ifPresent(System.out::println);  // a1

        //let's say that I have list of String and i wanto to convert them into List of Integer, for number format exception, it should return 1

        List<String> input = Arrays.asList("7","5","2","10","ank","3","mnk");
        List<Integer> output = input.stream().map(ele ->{
            try
            {
                return Integer.parseInt(ele);
            }
            catch (Exception e){
                return 1;
            }
        }).sorted(Integer::compareTo).collect(Collectors.toList());

        System.out.println("output is: " + output);

        //please remember that whenever we are using any map/filter or any intermediate transformations and using{} then use return statement

        //List of integers only
        List<String> inputInt = Arrays.asList("7","5","2","10","3","0");
        List<Integer> outputInt = inputInt.stream().map(Integer::parseInt).sorted().collect(Collectors.toList());
        System.out.println(outputInt);


        //if you want to sort array
        int a[] = {3,2,1,4};
        System.out.println("without sorting: " + Arrays.toString(a));
        int afSort[] = Arrays.stream(a).sorted().toArray();
        System.out.println("after sorting: " + Arrays.toString(afSort));

        /*if you want to sort it in reverse order, then we have to use Comparator.
          But Comparator works with Object, but Arrays.Stream produces an IntStream(a stream of primitive
          int values), so we have to convert int to Integer using boxed.
          ==>boxed : convert int to Integer
             mapToInt: convert Integer to int
          */

        int reverseSort[] = Arrays.stream(a).boxed().sorted(Comparator.reverseOrder())
                .mapToInt(Integer::intValue).toArray();
        System.out.println(Arrays.toString(reverseSort));





    }
}
