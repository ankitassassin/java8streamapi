package com.ankit.Example1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ListSorting6
{
    public static void main(String[] args) {

        List<Integer> li = Arrays.asList(50,26,76,21,100,61);

        li.stream().sorted((o1, o2) -> o2.compareTo(o1)).forEach(ele-> System.out.print(ele + ","));
        System.out.println();

        li.stream().sorted(Comparator.reverseOrder()).forEach(ele-> System.out.print(ele + ","));
        System.out.println();
        System.out.println("-----------------------");

        li.stream().sorted((o1, o2) -> o1.compareTo(o2)).forEach(ele-> System.out.print(ele + ","));
        System.out.println();

        li.stream().sorted(Integer::compareTo).forEach(ele-> System.out.print(ele + ","));


    }
}
