package com.ankit.Example1;

import java.util.stream.Stream;

public class Examples3
{
    public void meth()
    {
        /*An important characteristic of intermediate operations is laziness.
        Look at this sample where a terminal operation is missing:*/
        /*When executing this code snippet, nothing is printed to the console.
        That is because intermediate operations will only be executed when a terminal operation is present.*/

        Stream.of("d2", "a2", "b1", "b3", "c")
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return true;
                });

        //Now below code will print on console.
        Stream.of("d2", "a2", "b1", "b3", "c")
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return true;
                })
                .forEach(s -> System.out.println("forEach: " + s));

             /*    output:
                    filter:  d2
                    forEach: d2
                    filter:  a2
                    forEach: a2
                    filter:  b1
                    forEach: b1
                    filter:  b3
                    forEach: b3
                    filter:  c
                    forEach: c*/

        //another example
        System.out.println("==========================");
        Stream.of("d2", "a2", "b1", "b3", "c","a6")
                .map(s -> {
                    System.out.println("map: " + s);
                    return s.toUpperCase();
                })
                .anyMatch(s -> {
                    System.out.println("anyMatch: " + s);
                    return s.startsWith("A");
                });

   /* Note:
        anyMatch: It is a terminal operation. This method checks whether any element of the stream matches the given predicate. It returns
                  true if atleast one element matches the predicate, otherwise it reurns false.
                  It is a short circuting operation, meaning it stops processing once it finds a matching element.

                   output:
                     map: d2
                     anyMatch: D2
                     map: a2
                     anyMatch: A2
*/

        System.out.println("==========================");

        //another example
        Stream.of("d2", "a2", "b1", "b3", "c","a6")
                .map(String::toUpperCase)
                .anyMatch(s -> {
                    System.out.println("anyMatch: " + s);
                    return s.startsWith("A");
                });

                     /*output:
                        anyMatch: D2
                        anyMatch: A2*/




    }
}
