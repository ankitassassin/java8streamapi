package com.ankit.matching5;

import java.util.Arrays;
import java.util.List;

public class Example
{
    public static void main(String[] args) {
            List<String> p = Arrays.asList("mayank","ankit");
            boolean res = p.stream().allMatch(e->e.equalsIgnoreCase("ankit"));
            System.out.println(res);

            p.forEach(e->{
                if(e.equalsIgnoreCase("ankit")){
                    System.out.println("true");
                }
            });

    }
}
